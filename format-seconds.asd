(defsystem     format-seconds
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Format durations in seconds as human-friendly strings"
  :defsystem-depends-on ("literate-lisp")
  :depends-on  (:cl-ppcre)
  :components  ((:org "format-seconds")))
