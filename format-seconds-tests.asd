(defsystem     format-seconds-tests
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Tests for format-seconds"
  :defsystem-depends-on ("literate-lisp")
  :depends-on  (:fiveam :format-seconds)
  :components  ((:org "format-seconds"
                      :around-compile compile-with-tests)))

(defun compile-with-tests (fn)
  (unwind-protect
       (progn (pushnew :test *features*)
              (funcall fn))
    (setf *features* (remove :test *features*))))
